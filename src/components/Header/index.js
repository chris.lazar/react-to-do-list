import React, { Component } from 'react';

import './index.css';

class Header extends Component {
  render() {
    return (
      <div className="Header">
        <h1>This is a fancy header 🔥 </h1>
      </div>
    )
  }
}

export default Header;
