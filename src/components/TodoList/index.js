import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TodoListItem from '../TodoListItem';
import './index.css';

class TodoList extends Component {

  render() {
    return (
      <div className="TodoList">
        <ul>
          {
            this.props.todos.map((todo, index) =>
              <TodoListItem
                key={todo.id}
                todo={todo}
                onRemoveTodo={this.props.onRemoveTodo}
                onToggleCompleted={this.props.onToggleCompleted}
              />
            )
          }
        </ul>
      </div>
    );
  }
}

TodoList.propTypes = {
  onToggleCompleted: PropTypes.func.isRequired,
  onRemoveTodo: PropTypes.func.isRequired,
  todos: PropTypes.array
}

export default TodoList
